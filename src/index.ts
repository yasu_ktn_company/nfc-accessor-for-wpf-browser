interface External {
  helo();
  clearIdm();
}

class NfcAccessorForWpfBrowser {
  private static broadcastGetIdm: { (idm: string): void };
  private static isInit = false;

  static init(callback: { (idm: string): void }) {
    NfcAccessorForWpfBrowser.broadcastGetIdm = callback;
  }

  static helo() {
    try {
      window.external.helo();
    } catch(ex) {
      console.log('nfc-accessor is not support browser.');
    }
  }
  static clearIdm() {
    if (NfcAccessorForWpfBrowser.isInit) {
      window.external.clearIdm();
    }
  }

  static eventHelo() {
    NfcAccessorForWpfBrowser.isInit = true;
  }
  static eventNoticeIdm(idm: string) {
    if (NfcAccessorForWpfBrowser.isInit && NfcAccessorForWpfBrowser.broadcastGetIdm) {
      NfcAccessorForWpfBrowser.broadcastGetIdm(idm);
    }
  }
}

function eventHelo() {
  NfcAccessorForWpfBrowser.eventHelo();
}

function eventNoticeIdm(idm: string) {
  NfcAccessorForWpfBrowser.eventNoticeIdm(idm);
}

NfcAccessorForWpfBrowser.helo();
