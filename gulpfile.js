var gulp = require('gulp');
var typescript = require('gulp-typescript');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var del = require('del');

gulp.task('build:min', function() {
  return gulp.src(['src/**/*.ts'])
    .pipe(typescript())
    .pipe(uglify({
      beautify:true,
      preserveComments: 'some'
    }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('./dist/src/js'));
});

gulp.task('build', ['build:min'], function() {
  return gulp.src(['src/**/*.ts'])
    .pipe(typescript({
      declaration: true
    }))
    .pipe(gulp.dest('./dist/src/js'))
});

gulp.task('clean', function() {
  return del(['dist']);
});

gulp.task('default', ['clean'], function() {
  gulp.start('build');
});
