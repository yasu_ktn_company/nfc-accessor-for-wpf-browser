var NfcAccessorForWpfBrowser = (function () {
    function NfcAccessorForWpfBrowser() {
    }
    NfcAccessorForWpfBrowser.init = function (callback) {
        NfcAccessorForWpfBrowser.broadcastGetIdm = callback;
    };
    NfcAccessorForWpfBrowser.helo = function () {
        try {
            window.external.helo();
        }
        catch (ex) {
            console.log('nfc-accessor is not support browser.');
        }
    };
    NfcAccessorForWpfBrowser.clearIdm = function () {
        if (NfcAccessorForWpfBrowser.isInit) {
            window.external.clearIdm();
        }
    };
    NfcAccessorForWpfBrowser.eventHelo = function () {
        NfcAccessorForWpfBrowser.isInit = true;
    };
    NfcAccessorForWpfBrowser.eventNoticeIdm = function (idm) {
        if (NfcAccessorForWpfBrowser.isInit && NfcAccessorForWpfBrowser.broadcastGetIdm) {
            NfcAccessorForWpfBrowser.broadcastGetIdm(idm);
        }
    };
    return NfcAccessorForWpfBrowser;
}());
NfcAccessorForWpfBrowser.isInit = false;
function eventHelo() {
    NfcAccessorForWpfBrowser.eventHelo();
}
function eventNoticeIdm(idm) {
    NfcAccessorForWpfBrowser.eventNoticeIdm(idm);
}
NfcAccessorForWpfBrowser.helo();
