interface External {
    helo(): any;
    clearIdm(): any;
}
declare class NfcAccessorForWpfBrowser {
    private static broadcastGetIdm;
    private static isInit;
    static init(callback: {
        (idm: string): void;
    }): void;
    static helo(): void;
    static clearIdm(): void;
    static eventHelo(): void;
    static eventNoticeIdm(idm: string): void;
}
declare function eventHelo(): void;
declare function eventNoticeIdm(idm: string): void;
